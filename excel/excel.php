<?php

require("database/db.php");

function getExcelData($file_tmp, $filename) {
   	move_uploaded_file($file_tmp, $filename);

	$inputFileName  =   $filename;
	$inputFileType = 'Xlsx';
	$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
	$spreadsheet = $reader->load($inputFileName);
	$worksheet = $spreadsheet->getActiveSheet();
	$rows = $worksheet->toArray();
	file_put_contents("filename", print_r($rows, true));


	$servername = "localhost";      //questi sono i dati richiesti per l'accesso al database, al momento lavoriamo in locale con PhpMyAdmin
	$username = "root";
	$password = "";
	$db = "pare";

	$conn = mysqli_connect($servername, $username, $password,$db);
	
	/*

        (
            [0] => Nome
            [1] => Cognome
            [2] => email
            [3] => Telefono
            [4] => Categoria
            [5] => Ultima visita
            [6] => Prossimo contatto
            [7] => Countdown
            [8] => Giorni da visita
            [9] => DESCRIZIONE
            [10] => MESSAGGIO
        )

        Array ( [0] => Nome [1] => Cognome [2] => email [3] => Telefono [4] => Categoria [5] => Ultima visita [6] => Prossimo contatto [7] => Countdown [8] => Giorni da visita [9] => DESCRIZIONE [10] => MESSAGGIO )

	*/
	
	// INSERT INTO `customers` (`id`, `name`, 'surname', `category`, `last_visit`, `note`, `email`, `phone`) VALUES (NULL, '', '', '', '', '', '');

	$i = 0;
	$c = 0;
	$j = 0;

	foreach($rows as $key => $value) {

		$i++;
		$j++;

		if($i==1)
			continue;

		$name = $value[0];
		$surname = $value[1];
		$category = $value[4];
		$last_visit = $value[5];
		$last_visit = getFormatDate($last_visit, $i);  //questa parte ragiona impostando la data come da listato in basso
		$note = $value[10];
		$email = $value[2];
		$phone = $value[3];

        $check = "SELECT * FROM `scadenzario` WHERE email LIKE '$email'";
		$isPresent = select($check, $conn);
		if(count($isPresent) == 0)  {


			$sql = "INSERT INTO `scadenzario` (`ID`, `name`, `surname`, `category`, `last_visit`, `note`, `email`, `phone`) VALUES (NULL, '$name', '$surname', '$category', '$last_visit', '$note', '$email', '$phone');";

			query($sql, $conn);
			$c++;
		}
		
		else
		{$aggiornamento = "UPDATE scadenzario set name = '$name' WHERE 'email' LIKE '$email'";
	    query($aggiornamento, $conn);
        $c++;
		}

		if($j == 4)               //questa parte è provvisoria, serve per fermare il ciclo a 3 nomi inseriti e poi fermarsi
			return $c;
 
	};
	return $c;
}

// 10/12/2023
function getFormatDate($str, $i) {
	if(empty($str)) {
		die("Errore di parsing per la data di ultima visita: $str, riga $i");
	}
	$parts = explode("/", $str);
	if(count($parts) == 0) {
		die("Errore di parsing per la data di ultima visita: $str, riga $i");
	}
	$day = $parts[1];
	$month = $parts[0];             
	$year = $parts[2];
	$formatted = $year . "-" . $month . "-" . $day;     //modificato da / perché MySQL riceve solo il formato YYYY-MM-DD
	return $formatted;
}