<?php

function select($sql, $conn) {
	$result = $conn->query($sql);
	$rows = [];
	if ($result->num_rows > 0) {
	  // output data of each row
	  while($row = $result->fetch_assoc()) {
	    $rows []= $row;
	  }
	}
	return $rows;
}

function query($sql, $conn) {
	if ($conn->query($sql) != TRUE) {
	  echo "Error: " . $sql . "<br>" . $conn->error;
	}
}